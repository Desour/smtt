license for the textures (they are taken from minetest (https://github.com/minetest/minetest)):

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>

(one of the bubble files was modified with gimp by DS)

license for code:
cc0
