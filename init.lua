minetest.register_node("smtt:node", {
	description = "subfolder texture node",
	tiles = {"default_wood.png^smtt_bubble.png", "default_wood.png^smtt_heart.png"},
	groups = {choppy = 3, oddly_breakable_by_hand = 3},
})
